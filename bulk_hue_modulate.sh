#!/bin/bash

find $1 -type f -name "*.png" -exec magick {} -modulate 100,100,0 {} \;
