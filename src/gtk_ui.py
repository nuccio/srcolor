import sys
from . import config, tools, Gtk, Gio, color_set

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title=config.APP_NAME)
        self.colors = color_set.ColorSet()
        self.set_border_width(0)
        self.set_default_size(500, 500)

        # Header setup
        self.header = Gtk.HeaderBar(title=config.APP_NAME)
        self.header.props.show_close_button = True
        self.set_titlebar(self.header)

        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="document-save")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        button.set_tooltip_text("Save modifications and make backup")
        button.connect("clicked", self.on_save)
        self.header.pack_end(button)

        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="reload")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        button.set_tooltip_text("Reload colors from files")
        button.connect("clicked", self.load_color_list)
        self.header.pack_start(button)

        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="folder-open")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        button.set_tooltip_text("Open directory")
        button.connect("clicked", self.on_folder_clicked)
        self.header.pack_start(button)

        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="edit-undo")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        button.set_tooltip_text("Restore from last backup")
        button.connect("clicked", self.on_restore_last_backup)
        self.header.pack_start(button)

        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="edit-delete")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        button.set_tooltip_text("Delete backup files")
        button.connect("clicked", self.on_remove_backups)
        self.header.pack_start(button)

        # Layout
        self.scrolled = Gtk.ScrolledWindow()
        self.scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.listbox = Gtk.ListBox()
        self.scrolled.add(self.listbox)
        self.add(self.scrolled)
        self.show_all()

        # Try to load from first argument
        try:
            self.path = sys.argv[1]
        except IndexError:
            self.path = None
        else:
            self.load_color_list()

    def load_color_list(self, widget=None):
        if self.path is not None:
            Gtk.Container.remove(self.scrolled, self.listbox)
            self.listbox.destroy()
            self.listbox = Gtk.ListBox()
            self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)

            # Load colors from path
            config.debug("Searching colors in " + self.path)
            # Empty color_set
            self.colors = color_set.ColorSet()
            # Populate color set
            self.colors = tools.walk_for_colors(self.path, self.colors)

            self.header.set_subtitle("Search & replace colors in " + self.path)
            self.populate_listbox(self.listbox)
            self.scrolled.add(self.listbox)
            self.show_all()

    def on_save(self, widget):
        if self.colors is not None:
            modified_files = set()
            modified_colors = []
            for c in self.colors.colors:
                if c["new_r"] != None:
                    modified_files |= set(c["from_files"])
                    modified_colors.append(c)
            modified_files = list(modified_files)
            desc = "\n".join(modified_files)

            if len(modified_files) > 0:
                dialog = Gtk.MessageDialog(
                    transient_for=self,
                    flags=0,
                    message_type=Gtk.MessageType.WARNING,
                    buttons=Gtk.ButtonsType.OK_CANCEL,
                    text="WARNING: Do you really want to save your changes ?",
                )
                dialog.format_secondary_text(
                    """Backing up and modifying the following files:\n""" + desc
                )
                response = dialog.run()
                if response == Gtk.ResponseType.OK:
                    tools.replace_colors_in_files(modified_files, modified_colors)
                    # Reload interface
                    self.load_color_list()

                dialog.destroy()

    def on_remove_backups(self, widget):
        if self.path is not None:
            to_remove = tools.find_backups(self.path)

            if len(to_remove) > 0:
                desc = "\n".join(to_remove)
                dialog = Gtk.MessageDialog(
                    transient_for=self,
                    flags=0,
                    message_type=Gtk.MessageType.WARNING,
                    buttons=Gtk.ButtonsType.OK_CANCEL,
                    text="WARNING: Do you really want to permanently delete these\
backup files ?",
                )
                dialog.format_secondary_text(
                   desc
                )
                response = dialog.run()
                if response == Gtk.ResponseType.OK:
                    tools.remove_backups(to_remove)

                dialog.destroy()

    def on_restore_last_backup(self, widget):
        if self.path is not None:
            # Find most recent backups
            last_b = tools.find_last_backups(tools.find_backups(self.path))
            if last_b is not None:
                desc = "\n".join(last_b[1])
                dialog = Gtk.MessageDialog(
                    transient_for=self,
                    flags=0,
                    message_type=Gtk.MessageType.WARNING,
                    buttons=Gtk.ButtonsType.OK_CANCEL,
                    text="WARNING: Do you really want to overwrite these files\
and lose your modifications ?",
                )
                dialog.format_secondary_text(
                   desc
                )
                response = dialog.run()
                if response == Gtk.ResponseType.OK:
                    tools.restore_backup(last_b)
                    # Reload interface
                    self.load_color_list()

                dialog.destroy()

    def on_folder_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a folder",
            parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK
        )
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.path = dialog.get_filename()
            config.debug("Folder selected: " + self.path)
            # Reload interface
            self.load_color_list()

        dialog.destroy()

    def on_color_set(self, widget, data):
        self.colors.set_new_color(data["color"], widget.get_rgba())

    def on_color_reset(self, widget, data):
        button = data["target"]
        color = data["color"]
        button.set_rgba(color["gdk_rgba"])
        self.colors.reset_color(color)

    def on_draw_color_rect(self, widget, cr, data):
        # Draw colored rectangle
        context = widget.get_style_context()

        width = widget.get_allocated_width()
        height = widget.get_allocated_height()
        Gtk.render_background(context, cr, 0, 0, width, height)

        r, g, b, a = data["color"]
        cr.set_source_rgba(r, g, b, a)
        cr.rectangle(0, 0, width, height)
        cr.fill()

    def color_reset_button(self, color):
        button = Gtk.Button()

        area = Gtk.DrawingArea()
        area.set_size_request(24, 24)
        area.connect("draw", self.on_draw_color_rect, {"color": color["gdk_rgba"]})
        button.add(area)

        # Add details in tooltip
        file_list = "\n".join(map(str, color["from_files"]))
        button.set_tooltip_text(file_list)

        return button

    def color_choose_button(self, color):
        button = Gtk.ColorButton()
        button.set_size_request(96, 24)
        # Directly show color editor (no intermediate color palette dialog)
        button.props.show_editor = True
        button.set_rgba(color["gdk_rgba"])
        button.connect("color-set", self.on_color_set, {"color": color["gdk_rgba"]})

        return button

    def populate_listbox(self, listbox):
        config.debug("Building UI color list")
        for color in self.colors.colors:
            row = Gtk.ListBoxRow()
            hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
            row.add(hbox)
            button_reset = self.color_reset_button(color)
            hbox.add(button_reset)
            color_string = f'{color["gdk_rgba"].to_string()}'
            desc = color_string + " x " + str(color["occurences"])
            label = Gtk.Label(label=desc)
            hbox.add(label)
            button_choose = self.color_choose_button(color)
            hbox.pack_end(button_choose, False, True, 0)
            button_reset.connect("clicked", self.on_color_reset,
                    {"target": button_choose, "color": color})
            self.listbox.add(row)


