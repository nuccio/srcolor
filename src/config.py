APP_NAME = "srcolor"

# Verbosity of debug printing
VERBOSITY = 2

def debug(string, level=1):
    if level <= VERBOSITY:
        print(string)

