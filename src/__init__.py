from . import config

# Import Gtk wrapper
import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gdk, Gtk, Gio

from . import gtk_ui

def main():
    # GTK window display
    config.debug("Launching GTK UI")
    win = gtk_ui.MainWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()

