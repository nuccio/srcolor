'''Generic functions for file crawling, color parsing...'''

import os, re, shutil
from datetime import datetime

from . import config

REGEX_HEX = "#[0-9a-fA-F]{6}|#[0-9a-fA-F]{3}(?!\w)"
REGEX_RGB = "[Rr][Gg][Bb]\( *\d+%? *, *\d+%? *, *\d+%? *\)"
REGEX_RGBA = "[Rr][Gg][Bb][Aa]\( *\d+%? *, *\d+%? *, *\d+%? *, *\d+.?\d*%? *\)"

def find_color_strings(file):
    # Extract color strings from file
    colors = set()
    with open(file) as f:
        for line in f:
            # Match regexes for hexa, rgb, rgba
            color = re.findall(REGEX_HEX, line)
            color.extend(re.findall(REGEX_RGB, line))
            color.extend(re.findall(REGEX_RGBA, line))
            if len(color) > 0:
                desc = " ".join(map(str, color))
                config.debug("Parsed colors: " + desc, 2)
                colors |= set(color)
    return colors

def is_text(filename):
    # Differentiate text VS binary files
    with open(filename, "r") as file:
        try:
            head = file.read(512)
        except UnicodeDecodeError:
            return False
        return True

REGEX_BACKUPNAME = f"\.{config.APP_NAME}_backup$"
def is_backup_file(path):
    # Detect backups from our program
    m = re.search(REGEX_BACKUPNAME, path)
    if m is not None:
        return True
    return False

def walk_for_colors(path, color_set):
    # Traverse root directory for text files
    for root, dirs, files in os.walk(path):
        for file in files:
            path = os.path.join(root, file)
            if is_text(path) and not is_backup_file(path):
                # Extract color strings
                color_set.add_colors(find_color_strings(path), path)
    color_set.sort_palette()
    return color_set

def equal_rgb(c1, c2):
    if c1.red == c2.red and c1.green == c2.green and c1.blue == c2.blue:
        return True
    else:
        return False

def rgb_to_hex(rgb):
    # Convert rgb triplet to hexadecimal
    return '#%02x%02x%02x' % rgb

def translate_color(color, str_repr):
    # Identify color representation type (hexa, rgb or rgba)
    # then create according representation from new values
    if re.match(REGEX_HEX, str_repr) is not None:
        return rgb_to_hex((color["new_r"], color["new_g"], color["new_b"]))
    elif re.match(REGEX_RGB, str_repr) is not None:
        return f'rgb({color["new_r"]}, {color["new_g"]}, {color["new_b"]})'
    elif re.match(REGEX_RGBA, str_repr) is not None:
        alpha = str_repr.split(",")
        alpha = re.sub("[ \)]+", "", alpha[-1])
        return f'rgba({color["new_r"]}, {color["new_g"]},\
{color["new_b"]}, {alpha})'

def backup_file(path, date):
    shutil.move(path, path+date+".srcolor_backup")

def remove_backups(paths):
    for path in paths:
        if is_backup_file(path):
            config.debug("Removing "+path, 2)
            os.remove(path)

def find_backups(path):
    # Make list of backup files in tree
    paths = []
    for root, dirs, files in os.walk(path):
        for file in files:
            path = os.path.join(root, file)
            if is_backup_file(path):
                paths.append(path)
    return paths

LEN_BACKUP_SUFFIX = len(f"2022-11-11T11:11:11.111111.{config.APP_NAME}_backup")
def find_last_backups(paths):
    # Keep only most recent backups from list
    output = []
    most_recent = None
    for path in paths:
        if is_backup_file(path):
            prefix, suffix = path[:-LEN_BACKUP_SUFFIX], path[-LEN_BACKUP_SUFFIX:]
            if most_recent is None:
                most_recent = (suffix, [prefix])
            elif most_recent[0] < suffix:
                most_recent = (suffix, [prefix])
            elif most_recent[0] == suffix:
                most_recent[1].append(prefix)
    return most_recent

def restore_backup(most_recent):
    # Restore most recent backup
    for prefix in most_recent[1]:
        path = prefix + most_recent[0]
        config.debug("Removing "+prefix)
        os.remove(prefix)
        config.debug("Restoring "+path)
        shutil.move(path, prefix)

def replace_colors_in_files(paths, colors):
    now = datetime.now()
    now = now.strftime("%Y-%m-%dT%H:%M:%S.%f")
    for path in paths:
        with open(path, "r") as file:
            content = file.read()
            for color in colors:
                for r in color["str_repr"]:
                    new_repr = translate_color(color, r)
                    config.debug(f"Replacing {r} with {new_repr} in {path}", 2)
                    content = content.replace(r, new_repr)
        # Backup file
        backup_file(path, now)
        # Write new file
        with open(path, "w") as file:
            config.debug("Writing new " + path)
            file.write(content)


