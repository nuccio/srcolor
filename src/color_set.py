'''Object to manage a set of colors, in a set of files'''

import colorsys

from . import tools, Gdk

class ColorSet():
    # Regroup according to RGB (not alpha)
    # Keep origin files and original str representations
    # Count number of occurences
    # Depends on GDK color object
    def __init__(self):
        self.colors = []

    def add_color(self, string, file):
        gdk_rgba = Gdk.RGBA()
        gdk_rgba.parse(string)
        for c in self.colors:
            if tools.equal_rgb(c["gdk_rgba"], gdk_rgba):
                # Not a new color
                c["occurences"] += 1
                if string not in c["str_repr"]:
                   c["str_repr"].append(string)
                if file not in c["from_files"]:
                    c["from_files"].append(file)
                if gdk_rgba.alpha not in c["alphas"]:
                    c["alphas"].append(gdk_rgba.alpha)
                return
        # A new color
        r = gdk_rgba.red
        g = gdk_rgba.green
        b = gdk_rgba.blue
        a = gdk_rgba.alpha
        # Get hue, luminosity and saturation
        h, l, s = colorsys.rgb_to_hls(r, g, b)
        # Ignore alpha
        gdk_rgba.alpha = 1
        self.colors.append({
            "gdk_rgba": gdk_rgba,
            "from_files": [file],
            "str_repr": [string],
            "r": r,
            "g": g,
            "b": b,
            "alphas": [a],
            "h": h,
            "l": l,
            "s": s,
            "occurences": 1,
            "new_r": None,
            "new_g": None,
            "new_b": None
            })

    def add_colors(self, colors, file):
        for c in colors:
            self.add_color(c, file)

    def sort_palette(self):
        # Saturated first
        high_saturation = []
        low_saturation = []
        no_saturation = []
        for c in self.colors:
            if c["s"] > 0.3:
                high_saturation.append(c)
            elif c["s"] > 0:
                low_saturation.append(c)
            else:
                no_saturation.append(c)
        # Saturated are sorted by hue
        high_saturation = sorted(high_saturation, key=lambda c: c["h"])
        # Low saturated sorted by luminosity
        low_saturation = sorted(low_saturation, key=lambda c: c["l"])
        no_saturation = sorted(no_saturation, key=lambda c: c["l"])
        self.colors = high_saturation + low_saturation + no_saturation

    def set_new_color(self, old, new):
        for c in self.colors:
            if tools.equal_rgb(c["gdk_rgba"], old):
                # Convert gdk rgb to integer rgb
                c["new_r"] = int(255 * new.red)
                c["new_g"] = int(255 * new.green)
                c["new_b"] = int(255 * new.blue)
                break

    def reset_color(self, color):
        for c in self.colors:
            if tools.equal_rgb(c["gdk_rgba"], color["gdk_rgba"]):
                c["new_r"] = None
                c["new_g"] = None
                c["new_b"] = None
                break


