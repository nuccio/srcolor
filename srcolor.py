#!/usr/bin/python

from src import main, config

# Verbosity of debug printing
config.VERBOSITY = 2

if __name__ == '__main__':
    main()

